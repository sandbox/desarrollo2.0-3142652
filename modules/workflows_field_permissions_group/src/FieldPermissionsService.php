<?php

namespace Drupal\workflows_field_permissions_group;

use Drupal\field_permissions\FieldPermissionsService as FieldPermissionsServiceBase;
use Drupal\field_permissions\Plugin\FieldPermissionTypeInterface;
use Drupal\workflows_field_permissions_group\Plugin\FieldPermissionType\CustomPermissionsWorkflowGroupInterface;

/**
 * Class FieldPermissionsService.
 *
 * @package Drupal\workflows_field_permissions
 */
class FieldPermissionsService extends FieldPermissionsServiceBase {

  /**
   * {@inheritdoc}
   */
  public function getAllPermissions() {
    $permissions = [];
    /** @var \Drupal\field\FieldStorageConfigInterface[] $fields */
    $fields = $this->entityTypeManager->getStorage('field_storage_config')->loadMultiple();
    foreach ($fields as $key => $field) {
      // Check if this plugin defines custom permissions.
      $permission_type = $this->fieldGetPermissionType($field);
      if ($permission_type !== FieldPermissionTypeInterface::ACCESS_PUBLIC) {
        $plugin = $this->permissionTypeManager->createInstance($permission_type, [], $field);
        if ($plugin instanceof CustomPermissionsWorkflowGroupInterface) {
          $permissions += $plugin->getPermissions();
        }
      }
    }
    return $permissions;
  }

}
