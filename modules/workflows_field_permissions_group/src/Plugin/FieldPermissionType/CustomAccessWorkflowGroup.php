<?php

namespace Drupal\workflows_field_permissions_group\Plugin\FieldPermissionType;

use Drupal\Core\Access\AccessResultAllowed;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\ReplaceCommand;
use Drupal\Core\Annotation\Translation;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageException;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\field\FieldConfigInterface;
use Drupal\field_permissions\Annotation\FieldPermissionType;
use Drupal\field_permissions\FieldPermissionsService;
use Drupal\field_permissions\Plugin\AdminFormSettingsInterface;
use Drupal\field_permissions\Plugin\CustomPermissionsInterface;
use Drupal\field_permissions\Plugin\FieldPermissionType\Base;
use Drupal\group\Access\CalculatedGroupPermissionsInterface;
use Drupal\group\Access\CalculatedGroupPermissionsItemInterface;
use Drupal\group\Access\GroupAccessResult;
use Drupal\group\Entity\GroupContent;
use Drupal\group\Entity\GroupContentType;
use Drupal\group\Entity\GroupInterface;
use Drupal\group\Entity\GroupType;
use Drupal\group\Entity\GroupTypeInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\user\RoleStorageInterface;
use Drupal\user\UserInterface;
use Drupal\workflows\Entity\Workflow;
use Drupal\workflows\State;
use Drupal\workflows\WorkflowInterface;
use Drupal\workflows_field_permissions\Plugin\FieldPermissionType\CustomAccessWorkflow;
use spec\Behat\MinkExtension\Listener\SessionsListenerSpec;

/**
 * Class CustomAccessGroupWorkflow.
 *
 * @package Drupal\workflows_field_permissions_group\Plugin\FieldPermissionType
 *
 * @FieldPermissionType(
 *   id = "custom_workflow_group",
 *   title = @Translation("Custom permissions by workflow on a group"),
 *   description = @Translation("Define custom permissions based on the
 *   workflow state of a group where this entity belongs to."),
 *   weight = 52
 * )
 */
class CustomAccessWorkflowGroup extends Base implements CustomPermissionsInterface, CustomPermissionsWorkflowGroupInterface, AdminFormSettingsInterface {

  /**
   * @inheritDoc
   */
  public function hasFieldAccess($operation, EntityInterface $entity, AccountInterface $account) {
    assert(in_array($operation, [
      "edit",
      "view",
    ]), 'The operation is either "edit" or "view", "' . $operation . '" given instead.');

    $field_name = $this->fieldStorage->getName();
    $workflow_field = $this->fieldStorage->getThirdPartySetting('workflows_field_permissions', 'group_workflow_field');
    $group_type = $this->fieldStorage->getThirdPartySetting('workflows_field_permissions', 'group_type');
    $state_none = FALSE;
    $group = null;

    if (!empty($workflow_field)) {
      $workflow = CustomAccessWorkflow::getWorkflowFromFieldId($workflow_field);
      if ($workflow instanceof WorkflowInterface) {

        if ($entity->isNew()) {
          // Try to get the group through the URL. That means we are creating
          // the entity directly on the group.
          /** @var \Drupal\Core\Routing\CurrentRouteMatch $current_route_match */
          $current_route_match = \Drupal::service('current_route_match');
          $group = $current_route_match->getParameter('group');
        } else {
          $group_content_entities = GroupContent::loadByEntity($entity);
          /** @var \Drupal\group\Entity\GroupContentInterface $group_content_entity */
          foreach ($group_content_entities as $group_content_entity) {
            if ($group_content_entity->getGroup()->bundle() == $group_type) {
              $group = $group_content_entity->getGroup();
              break;
            }
          }
        }

        // That means the entity is not in a group of the desired type.
        if (!$group instanceof GroupInterface) {
          $state_none = TRUE;
        } else {
          list($entity_type, $bundle, $workflow_field_name) = explode('.', $workflow_field);
          if ($group->hasField($workflow_field_name)) {
            if ($group->get($workflow_field_name)->isEmpty()) {
              $state_none = TRUE;
            } else {
              $state = $group->get($workflow_field_name)->value;

              if ($operation === 'edit' && $entity->isNew()) {
                return GroupAccessResult::allowedIfHasGroupPermission($group, $account, 'create ' . $field_name . ' on workflow ' . $workflow->id() . ' state ' . $state)->isAllowed();
              } else {
                // Get the fields referencing this workflow.
                if (GroupAccessResult::allowedIfHasGroupPermission($group, $account, $operation . ' ' . $field_name . ' on workflow ' . $workflow->id() . ' state ' . $state)->isAllowed()) {
                  return TRUE;
                }
                else {
                  // User entities don't implement `EntityOwnerInterface`.
                  if ($entity instanceof UserInterface) {
                    return $entity->id() == $account->id() && GroupAccessResult::allowedIfHasGroupPermission($group, $account,$operation . ' own ' . $field_name . ' on workflow ' . $workflow->id() . ' state ' . $state)->isAllowed();
                  }
                  elseif ($entity instanceof EntityOwnerInterface) {
                    return $entity->getOwnerId() == $account->id() && GroupAccessResult::allowedIfHasGroupPermission($group, $account,$operation . ' own ' . $field_name . ' on workflow ' . $workflow->id() . ' state ' . $state)->isAllowed();
                  }
                }
              }
            }
          }
        }
      }
    }

    // TODO: load group by context on new unsaved entities.
    if ($state_none) {
      if ($group instanceof GroupInterface) {
        if ($entity->isNew()) {
          return GroupAccessResult::allowedIfHasGroupPermission($group, $account, 'create ' . $field_name . ' on workflow ' . $workflow->id() . ' state none')->isAllowed();
        }
        else {
          if (GroupAccessResult::allowedIfHasGroupPermission($group, $account, $operation . ' ' . $field_name . ' on workflow ' . $workflow->id() . ' state none')->isAllowed()) {
            return TRUE;
          }
          else {
            // User entities don't implement `EntityOwnerInterface`.
            if ($entity instanceof UserInterface) {
              return $entity->id() == $account->id() && GroupAccessResult::allowedIfHasGroupPermission($group, $account,$operation . ' own ' . $field_name . ' on workflow none' . $workflow->id() . ' state none')->isAllowed();
            }
            elseif ($entity instanceof EntityOwnerInterface) {
              return $entity->getOwnerId() == $account->id() && GroupAccessResult::allowedIfHasGroupPermission($group, $account,$operation . ' own ' . $field_name . ' on workflow ' . $workflow->id() . ' state none')->isAllowed();
            }
          }
        }
      }
      else {
        // If there is no group, calculate the permissions.
        /** @var CalculatedGroupPermissionsInterface $calculated_permissions */
        $calculated_permissions = \Drupal::service('group_permission.chain_calculator')
          ->calculatePermissions($account);

        $calculated_permissions_item = $calculated_permissions->getItem('group_type', $group_type);
        if ($calculated_permissions_item instanceof CalculatedGroupPermissionsItemInterface) {
          if ($entity->isNew()) {
            return $calculated_permissions_item->hasPermission('create ' . $field_name . ' on workflow ' . $workflow->id() . ' state none');
          }
          else {
            if ($calculated_permissions_item->hasPermission($operation . ' ' . $field_name . ' on workflow ' . $workflow->id() . ' state none')) {
              return TRUE;
            }
            else {
              // User entities don't implement `EntityOwnerInterface`.
              if ($entity instanceof UserInterface) {
                return $entity->id() == $account->id() && $calculated_permissions_item->hasPermission($operation . ' own ' . $field_name . ' on workflow none' . $workflow->id() . ' state none');
              }
              elseif ($entity instanceof EntityOwnerInterface) {
                return $entity->getOwnerId() == $account->id() && $calculated_permissions_item->hasPermission($operation . ' own ' . $field_name . ' on workflow ' . $workflow->id() . ' state none');
              }
            }
          }
        }
      }
    }

    // Default to deny since access can be explicitly granted (edit field_name),
    // even if this entity type doesn't implement the EntityOwnerInterface.
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function hasFieldViewAccessForEveryEntity(AccountInterface $account) {
    $field_name = $this->fieldStorage->getName();
    return $account->hasPermission('view ' . $field_name);
  }

  /**
   * {@inheritdoc}
   */
  public function buildAdminForm(array &$form, FormStateInterface $form_state, RoleStorageInterface $role_storage) {
    /** @var \Drupal\Core\Entity\FieldableEntityInterface $entity */
    $entity = $form['#entity'];

    $form['custom_workflow_group'] = [
      '#type' => 'container',
      '#states' => [
        'visible' => [
          ':input[name="type"]' => ['value' => 'custom_workflow_group'],
        ],
      ],
    ];

    $groupTypeOptions = [
      '' => t('Please select one...'),
    ];
    foreach (GroupContentType::loadByEntityTypeId($entity->getEntityTypeId()) as $groupContentType) {
      if ($groupContentType->getContentPlugin()->getEntityBundle() === $entity->bundle()) {
        $groupType = $groupContentType->getGroupType();
        $groupTypeOptions[$groupType->id()] = $groupType->label();
      }
    }

    $form['custom_workflow_group']['group_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Group type'),
      '#options' => $groupTypeOptions,
      '#default_value' => $this->fieldStorage->getThirdPartySetting('workflows_field_permissions', 'group_type'),
      // @TODO ensure ajax callback get the the default values.
      '#ajax' => [
        'callback' => [$this, 'addWorkflowField'],
        'wrapper' => 'group-workflow-field-wrapper',
      ],
    ];

    $form['custom_workflow_group']['group_workflow_field_wrapper'] = [
      '#type' => 'container',
      '#attributes' => ['id' => 'group-workflow-field-wrapper'],
    ];

    if ($form_state->hasValue('group_type')) {
      $group_type_id = $form_state->getValue('group_type');
    }
    else {
      $group_type_id = $form['custom_workflow_group']['group_type']['#default_value'];
    }

    if (!empty($group_type_id)) {
      $definitions = \Drupal::service('entity_field.manager')
        ->getFieldDefinitions('group', $group_type_id);

      $workflowFieldOptions = [
        '' => t('Please select one...'),
      ];

      foreach ($definitions as $fieldDefinition) {
        if ($fieldDefinition->getType() == 'workflows_field_item') {
          $workflowFieldOptions[$fieldDefinition->id()] = $fieldDefinition->getLabel();
        }
      }

      $form['custom_workflow_group']['group_workflow_field_wrapper']['group_workflow_field'] = [
        '#type' => 'select',
        '#title' => t('Workflow field'),
        '#options' => $workflowFieldOptions,
        '#default_value' => $this->fieldStorage->getThirdPartySetting('workflows_field_permissions', 'group_workflow_field'),
        // @TODO ensure ajax callback get the the default values.
        '#ajax' => [
          'callback' => [$this, 'addWorkflowField'],
          'wrapper' => 'group-workflow-field-wrapper',
        ],
      ];

      $form['custom_workflow_group']['group_workflow_field_wrapper']['permissions_wrapper'] = [
        '#type' => 'container',
        '#attributes' => ['id' => 'group-permissions-wrapper'],
      ];

      $this->addPermissionsGrid($form, $form_state);
    }

    // Attach the Drupal user permissions library.
    $form['#attached']['library'][] = 'user/drupal.user.permissions';
  }

  /**
   * Ajax callback to Add the workflow field.
   *
   * @param array $form
   *   The array form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return array
   *   The render array with the ajax wrapper.
   */
  public function addWorkflowField(array &$form, FormStateInterface $form_state) {
    return $form['custom_workflow_group']['group_workflow_field_wrapper'];
  }

  /**
   * Attach a permissions grid to the field edit form.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return array
   *   The renderable array.
   */
  public function addPermissionsGrid(array &$form, FormStateInterface $form_state) {
    // TODO: Build by ajax the permissions matrix based on the selected workflow.
    if ($form_state->hasValue('group_workflow_field')) {
      $workflow_field = $form_state->getValue('group_workflow_field');
    }
    else {
      $workflow_field = $form['custom_workflow_group']['group_workflow_field_wrapper']['group_workflow_field']['#default_value'];
    }

    if (!empty($workflow_field)) {
      $workflow = CustomAccessWorkflow::getWorkflowFromFieldId($workflow_field);
      if ($workflow instanceof WorkflowInterface) {

        // Create a fake state to achieve permissions when the entity has no
        // workflow state.
        $states = [new State($workflow->getTypePlugin(), 'none', t('No state'))];

        $states += $workflow->getTypePlugin()->getStates();

        $permissions = [];
        $field_name = $this->fieldStorage->getName();
        $permission_list = FieldPermissionsService::getList($field_name);
        $perms_name = array_keys($permission_list);
        foreach ($perms_name as $perm_name) {
          $name = $perm_name . ' ' . $field_name;
          $permissions[$name] = $permission_list[$perm_name];
        }

        if ($form_state->hasValue('group_type')) {
          $group_type_id = $form_state->getValue('group_type');
        }
        else {
          $group_type_id = $form['custom_workflow_group']['group_type']['#default_value'];
        }

        if (!empty($group_type_id)) {
          $roles = \Drupal::service('entity_type.manager')
            ->getStorage('group_role')
            ->loadByProperties([
              'group_type' => $group_type_id,
            ]);

          foreach ($states as $state) {
            $form['custom_workflow_group']['group_workflow_field_wrapper']['permissions_wrapper']['header__' . $state->id()] = [
              '#type' => 'markup',
              '#markup' => '<h3>' . $state->label() . '</h3>',
            ];
            $form['custom_workflow_group']['group_workflow_field_wrapper']['permissions_wrapper']['group_grid__' . $state->id()] = [
              '#type' => 'table',
              '#header' => [t('Permission')],
              '#attributes' => ['class' => ['permissions', 'js-permissions']],
              '#sticky' => TRUE,
            ];
            /** @var \Drupal\group\Entity\GroupRoleInterface $role */
            foreach ($roles as $role) {
              $form['custom_workflow_group']['group_workflow_field_wrapper']['permissions_wrapper']['group_grid__' . $state->id()]['#header'][] = [
                'data' => $role->label(),
                'class' => ['checkbox'],
              ];
            }

            foreach ($permissions as $provider => $permission) {
              $form['custom_workflow_group']['group_workflow_field_wrapper']['permissions_wrapper']['group_grid__' . $state->id()][$provider]['description'] = [
                '#type' => 'inline_template',
                '#template' => '<div class="permission"><span class="title">{{ title }}</span>{% if description or warning %}<div class="description">{% if warning %}<em class="permission-warning">{{ warning }}</em> {% endif %}{{ description }}</div>{% endif %}</div>',
                '#context' => [
                  'title' => $permission["title"],
                ],
              ];
              /** @var \Drupal\group\Entity\GroupRoleInterface $role */
              foreach ($roles as $name => $role) {
                $form['custom_workflow_group']['group_workflow_field_wrapper']['permissions_wrapper']['group_grid__' . $state->id()][$provider][$name] = [
                  '#title' => $name . ': ' . $permission["title"],
                  '#title_display' => 'invisible',
                  '#type' => 'checkbox',
                  '#attributes' => [
                    'class' => [
                      'rid-' . $name,
                      'js-rid-' . $name,
                    ],
                  ],
                  '#wrapper_attributes' => [
                    'class' => ['checkbox'],
                  ],
                ];
                $perm_name = $provider . ' on workflow ' . $workflow->id() . ' state ' . $state->id();
                $form['custom_workflow_group']['group_workflow_field_wrapper']['permissions_wrapper']['group_grid__' . $state->id()][$provider][$name]['#default_value'] = $role->hasPermission($perm_name);
              }
            }
          }
        }
      }
    }

    return $form['custom_workflow_group']['group_workflow_field_wrapper']['permissions_wrapper'];
  }

  /**
   * {@inheritdoc}
   */
  public function submitAdminForm(array &$form, FormStateInterface $form_state, RoleStorageInterface $role_storage) {

    $group_workflow_field = $form_state->getValue('group_workflow_field');
    $group_type = $form_state->getValue('group_type');
    $this->fieldStorage->setThirdPartySetting('workflows_field_permissions', 'group_type', $group_type);
    $this->fieldStorage->setThirdPartySetting('workflows_field_permissions', 'group_workflow_field', $group_workflow_field);
    $this->fieldStorage->save();

    $workflow = CustomAccessWorkflow::getWorkflowFromFieldId($group_workflow_field);
    if ($workflow instanceof WorkflowInterface) {
      $states = [new State($workflow->getTypePlugin(), 'none', t('No state'))];

      $states += $workflow->getTypePlugin()->getStates();

      /** @var \Drupal\group\Entity\GroupRoleInterface[] $roles */
      $roles = \Drupal::service('entity_type.manager')
        ->getStorage('group_role')
        ->loadByProperties([
          'group_type' => $group_type,
        ]);

      /** @var \Drupal\group\Entity\GroupRoleInterface $group_role */
      foreach ($states as $state) {
        $custom_permissions = $form_state->getValue('group_grid__' . $state->id());
        foreach ($custom_permissions as $op => $field_perm) {
          $permission_name = $op . ' on workflow ' . $workflow->id() . ' state ' . $state->id();
          foreach ($field_perm as $role_name => $role_permission) {
            // If using this plugin, set permissions to the value submitted in the
            // form. Otherwise remove all permissions as they will no longer exist.
            // @TODO revoke permission when the selected workflow had changed.
            if ($role_permission) {
              $roles[$role_name]->grantPermission($permission_name);
            }
            else {
              $roles[$role_name]->revokePermission($permission_name);
            }
          }
        }
      }

      // Save all roles.
      foreach ($roles as $role) {
        $role->trustData()->save();
      }
    }

  }

  /**
   * {@inheritdoc}
   */
  public function getPermissions() {
    $permissions = [];
    $field_name = $this->fieldStorage->getName();
    $permission_list = FieldPermissionsService::getList($field_name);
    $perms_name = array_keys($permission_list);
    $workflows = Workflow::loadMultiple();

    foreach ($workflows as $workflow) {
      if ($workflow instanceof WorkflowInterface) {
        $states = [new State($workflow->getTypePlugin(), 'none', t('No state'))];

        $states += $workflow->getTypePlugin()->getStates();
        foreach ($states as $state) {
          foreach ($perms_name as $perm_name) {
            $name = $perm_name . ' ' . $field_name . ' on workflow ' . $workflow->id() . ' state ' . $state->id();
            $permissions[$name] = [
              'title' => ucfirst($name),
            ];
          }
        }
      }
    }

    return $permissions;
  }

}
