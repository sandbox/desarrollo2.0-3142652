<?php

namespace Drupal\workflows_field_permissions_group\Plugin\FieldPermissionType;

use Drupal\field_permissions\Plugin\CustomPermissionsInterface;

/**
 * Interface CustomPermissionsWorkflowGroupInterface.
 *
 * @package Drupal\workflows_field_permissions_group\Plugin\FieldPermissionType
 */
interface CustomPermissionsWorkflowGroupInterface extends CustomPermissionsInterface {

}
