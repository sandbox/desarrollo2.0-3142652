<?php

namespace Drupal\workflows_field_permissions\Plugin\FieldPermissionType;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\field\FieldConfigInterface;
use Drupal\field_permissions\Annotation\FieldPermissionType;
use Drupal\field_permissions\FieldPermissionsService;
use Drupal\field_permissions\Plugin\AdminFormSettingsInterface;
use Drupal\field_permissions\Plugin\CustomPermissionsInterface;
use Drupal\field_permissions\Plugin\FieldPermissionType\Base;
use Drupal\user\EntityOwnerInterface;
use Drupal\user\RoleStorageInterface;
use Drupal\user\UserInterface;
use Drupal\workflows\Entity\Workflow;
use Drupal\workflows\State;
use Drupal\workflows\WorkflowInterface;

/**
 * Class CustomAccessGroupWorkflow.
 *
 * @package Drupal\workflows_field_permissions\Plugin\FieldPermissionType
 *
 * @FieldPermissionType(
 *   id = "custom_workflow",
 *   title = @Translation("Custom permissions by workflow"),
 *   description = @Translation("Define custom permissions based on the
 *   workflow state of this entity."),
 *   weight = 51
 * )
 */
class CustomAccessWorkflow extends Base implements CustomPermissionsInterface, AdminFormSettingsInterface {

  /**
   * @inheritDoc
   */
  public function hasFieldAccess($operation, EntityInterface $entity, AccountInterface $account) {
    assert(in_array($operation, [
      "edit",
      "view",
    ]), 'The operation is either "edit" or "view", "' . $operation . '" given instead.');

    $is_state_none = FALSE;

    $field_name = $this->fieldStorage->getName();
    $workflow_field = $this->fieldStorage->getThirdPartySetting('workflows_field_permissions', 'workflow_field');
    if (!empty($workflow_field)) {
      $workflow = self::getWorkflowFromFieldId($workflow_field);
      if ($workflow instanceof WorkflowInterface) {
        [
          $entity_type,
          $bundle,
          $workflow_field_name
        ] = explode('.', $workflow_field);
        if ($entity->hasField($workflow_field_name) && !$entity->get($workflow_field_name)
            ->isEmpty()) {
          $state = $entity->get($workflow_field_name)->value;
          // Get the fields referencing this workflow.
          if ($operation === 'edit' && $entity->isNew()) {
            return $account->hasPermission('create ' . $field_name . ' on workflow ' . $workflow->id() . ' state ' . $state);
          }
          if ($account->hasPermission($operation . ' ' . $field_name . ' on workflow ' . $workflow->id() . ' state ' . $state)) {
            return TRUE;
          }
          else {
            // User entities don't implement `EntityOwnerInterface`.
            if ($entity instanceof UserInterface) {
              return $entity->id() == $account->id() && $account->hasPermission($operation . ' own ' . $field_name . ' on workflow ' . $workflow->id() . ' state ' . $state);
            }
            elseif ($entity instanceof EntityOwnerInterface) {
              return $entity->getOwnerId() == $account->id() && $account->hasPermission($operation . ' own ' . $field_name . ' on workflow ' . $workflow->id() . ' state ' . $state);
            }
          }
        }
        else {
          $is_state_none = TRUE;
        }
      }
    }

    if ($is_state_none) {
      $state = 'none';
      if ($operation === 'edit' && $entity->isNew()) {
        return $account->hasPermission('create ' . $field_name . ' on workflow ' . $workflow->id() . ' state ' . $state);
      }
      if ($account->hasPermission($operation . ' ' . $field_name . ' on workflow ' . $workflow->id() . ' state ' . $state)) {
        return TRUE;
      }
      else {
        // User entities don't implement `EntityOwnerInterface`.
        if ($entity instanceof UserInterface) {
          return $entity->id() == $account->id() && $account->hasPermission($operation . ' own ' . $field_name . ' on workflow ' . $workflow->id() . ' state ' . $state);
        }
        elseif ($entity instanceof EntityOwnerInterface) {
          return $entity->getOwnerId() == $account->id() && $account->hasPermission($operation . ' own ' . $field_name . ' on workflow ' . $workflow->id() . ' state ' . $state);
        }
      }
    }

    // Default to deny since access can be explicitly granted (edit field_name),
    // even if this entity type doesn't implement the EntityOwnerInterface.
    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function hasFieldViewAccessForEveryEntity(AccountInterface $account) {
    $field_name = $this->fieldStorage->getName();
    return $account->hasPermission('view ' . $field_name);
  }

  /**
   * {@inheritdoc}
   */
  public function buildAdminForm(array &$form, FormStateInterface $form_state, RoleStorageInterface $role_storage) {
    /** @var \Drupal\Core\Entity\FieldableEntityInterface $entity */
    $entity = $form['#entity'];
    $workflowFieldOptions = [
      '' => t('Please select one...'),
    ];

    foreach ($entity->getFieldDefinitions() as $fieldDefinition) {
      if ($fieldDefinition->getType() == 'workflows_field_item') {
        $workflowFieldOptions[$fieldDefinition->id()] = $fieldDefinition->getLabel();
      }
    }

    $form['custom_workflow'] = [
      '#type' => 'container',
      '#states' => [
        'visible' => [
          ':input[name="type"]' => ['value' => 'custom_workflow'],
        ],
      ],
    ];

    $form['custom_workflow']['workflow_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Workflow field'),
      '#options' => $workflowFieldOptions,
      '#default_value' => $this->fieldStorage->getThirdPartySetting('workflows_field_permissions', 'workflow_field'),
      // @TODO ensure ajax callback get the the default values.
      '#ajax' => [
        'callback' => [$this, 'addPermissionsGrid'],
        'wrapper' => 'permissions-wrapper',
      ],
    ];

    $form['custom_workflow']['permissions_wrapper'] = [
      '#type' => 'container',
      '#attributes' => ['id' => 'permissions-wrapper'],
    ];

    // Attach the Drupal user permissions library.
    $form['#attached']['library'][] = 'user/drupal.user.permissions';

    self::addPermissionsGrid($form, $form_state);
  }

  /**
   * Attach a permissions grid to the field edit form.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   *
   * @return array
   *   The renderable array.
   */
  public function addPermissionsGrid(array &$form, FormStateInterface $form_state) {
    // TODO: Build by ajax the permissions matrix based on the selected workflow.
    if ($form_state->hasValue('workflow_field')) {
      $workflow_field = $form_state->getValue('workflow_field');
    }
    else {
      $workflow_field = $form['custom_workflow']['workflow_field']['#default_value'];
    }

    if (!empty($workflow_field)) {
      $workflow = self::getWorkflowFromFieldId($workflow_field);
      if ($workflow instanceof WorkflowInterface) {
        $states = [new State($workflow->getTypePlugin(), 'none', t('No state'))];
        $states += $workflow->getTypePlugin()->getStates();

        $permissions = [];
        $field_name = $this->fieldStorage->getName();
        $permission_list = FieldPermissionsService::getList($field_name);
        $perms_name = array_keys($permission_list);
        foreach ($perms_name as $perm_name) {
          $name = $perm_name . ' ' . $field_name;
          $permissions[$name] = $permission_list[$perm_name];
        }
        $roles = \Drupal::service('entity_type.manager')
          ->getStorage('user_role')
          ->loadMultiple();

        foreach ($states as $state) {
          $form['custom_workflow']['permissions_wrapper']['header__' . $state->id()] = [
            '#type' => 'markup',
            '#markup' => '<h3>' . $state->label() . '</h3>',
          ];
          $form['custom_workflow']['permissions_wrapper']['grid__' . $state->id()] = [
            '#type' => 'table',
            '#header' => [t('Permission')],
            '#attributes' => ['class' => ['permissions', 'js-permissions']],
            '#sticky' => TRUE,
          ];
          foreach ($roles as $role) {
            $form['custom_workflow']['permissions_wrapper']['grid__' . $state->id()]['#header'][] = [
              'data' => $role->label(),
              'class' => ['checkbox'],
            ];
          }
          // @todo Remove call to global service.
          $test = \Drupal::service('field_permissions.permissions_service')
            ->getPermissionsByRole();
          foreach ($permissions as $provider => $permission) {
            $form['custom_workflow']['permissions_wrapper']['grid__' . $state->id()][$provider]['description'] = [
              '#type' => 'inline_template',
              '#template' => '<div class="permission"><span class="title">{{ title }}</span>{% if description or warning %}<div class="description">{% if warning %}<em class="permission-warning">{{ warning }}</em> {% endif %}{{ description }}</div>{% endif %}</div>',
              '#context' => [
                'title' => $permission["title"],
              ],
            ];
            foreach ($roles as $name => $role) {
              $form['custom_workflow']['permissions_wrapper']['grid__' . $state->id()][$provider][$name] = [
                '#title' => $name . ': ' . $permission["title"],
                '#title_display' => 'invisible',
                '#type' => 'checkbox',
                '#attributes' => ['class' => ['rid-' . $name, 'js-rid-' . $name]],
                '#wrapper_attributes' => [
                  'class' => ['checkbox'],
                ],
              ];
              $perm_name = $provider . ' on workflow ' . $workflow->id() . ' state ' . $state->id();
              if (!empty($test[$name]) && in_array($perm_name, $test[$name])) {
                $form['custom_workflow']['permissions_wrapper']['grid__' . $state->id()][$provider][$name]['#default_value'] = in_array($perm_name, $test[$name]);
              }
              if ($role->isAdmin()) {
                $form['custom_workflow']['permissions_wrapper']['grid__' . $state->id()][$provider][$name]['#disabled'] = TRUE;
                $form['custom_workflow']['permissions_wrapper']['grid__' . $state->id()][$provider][$name]['#default_value'] = TRUE;
              }
            }
          }
        }
      }
    }

    return $form['custom_workflow']['permissions_wrapper'];
  }

  /**
   * {@inheritdoc}
   */
  public function submitAdminForm(array &$form, FormStateInterface $form_state, RoleStorageInterface $role_storage) {

    $workflow_field = $form_state->getValue('workflow_field');
    $this->fieldStorage->setThirdPartySetting('workflows_field_permissions', 'workflow_field', $workflow_field);
    $this->fieldStorage->save();

    $workflow = self::getWorkflowFromFieldId($workflow_field);
    if ($workflow instanceof WorkflowInterface) {
      $states = [new State($workflow->getTypePlugin(), 'none', t('No state'))];

      $states += $workflow->getTypePlugin()->getStates();

      /** @var \Drupal\user\RoleInterface[] $roles */
      $roles = [];

      foreach ($states as $state) {
        $custom_permissions = $form_state->getValue('grid__' . $state->id());
        foreach ($custom_permissions as $op => $field_perm) {
          $permission_name = $op . ' on workflow ' . $workflow->id() . ' state ' . $state->id();
          foreach ($field_perm as $role_name => $role_permission) {
            $roles[$role_name] = $role_storage->load($role_name);
            // If using this plugin, set permissions to the value submitted in the
            // form. Otherwise remove all permissions as they will no longer exist.
            $role_permission = $form_state->getValue('type') === $this->getPluginId() ? $role_permission : FALSE;
            // @TODO revoke permission when the selected workflow had changed.
            if ($role_permission) {
              $roles[$role_name]->grantPermission($permission_name);
            }
            else {
              $roles[$role_name]->revokePermission($permission_name);
            }
          }
        }
        // Save all roles.
        foreach ($roles as $role) {
          $role->trustData()->save();
        }
      }
    }


  }

  /**
   * {@inheritdoc}
   */
  public function getPermissions() {
    $permissions = [];
    $field_name = $this->fieldStorage->getName();
    $permission_list = FieldPermissionsService::getList($field_name);
    $perms_name = array_keys($permission_list);
    $workflows = Workflow::loadMultiple();
    foreach ($workflows as $workflow) {
      if ($workflow instanceof WorkflowInterface) {
        $states = [new State($workflow->getTypePlugin(), 'none', t('No state'))];

        $states += $workflow->getTypePlugin()->getStates();
        foreach ($states as $state) {
          foreach ($perms_name as $perm_name) {
            $name = $perm_name . ' ' . $field_name . ' on workflow ' . $workflow->id() . ' state ' . $state->id();
            $permissions[$name] = [
              'label' => t('@op @field_name on workflow <em>@workflow</em> state <em>@state</em>', [
                '@op' => ucfirst($perm_name),
                '@field_name' => $field_name,
                '@workflow' => $workflow->label(),
                '@state' => $state->label(),
              ]),
              'title' => t('@op @field_name on workflow <em>@workflow</em> state <em>@state</em>', [
                '@op' => ucfirst($perm_name),
                '@field_name' => $field_name,
                '@workflow' => $workflow->label(),
                '@state' => $state->label(),
              ]),
            ];
          }
        }
      }
    }

    return $permissions;
  }

  /**
   * Helper function to get the workflow entity used by a workflow field.
   *
   * @param $workflow_field
   *   The workflow field.
   *
   * @return WorkflowInterface|null
   *   The workflow entity or null in case of error.
   */
  public static function getWorkflowFromFieldId($workflow_field) {
    $workflow_field_parts = explode('.', $workflow_field);
    if (count($workflow_field_parts) == 3) {
      /** @var \Drupal\Core\Entity\EntityFieldManagerInterface $field_manager */
      $field_manager = \Drupal::service('entity_field.manager');
      $field_definitions = $field_manager->getFieldDefinitions($workflow_field_parts[0], $workflow_field_parts[1]);
      if (!empty($field_definitions[$workflow_field_parts[2]])) {
        $field_definition = $field_definitions[$workflow_field_parts[2]];
      }

      if ($field_definition instanceof FieldConfigInterface) {
        $workflow_id = $field_definition->getFieldStorageDefinition()
          ->getSetting('workflow');
        if ($workflow_id) {
          return Workflow::load($workflow_id);
        }
      }
    }
    return NULL;
  }

}
